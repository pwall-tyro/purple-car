package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.*;
import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Usability unit tests")
@WebMvcTest
public class appSpec {

    @Autowired
    private MockMvc mockMvc;

    /**
     * Sample unit test
     */
    @Test
    public void should_ReturnValidCarObject_whenValidInitilisation(){
        Car car = new Car(new CarId(1), new VIN("3FADP4EJ9BM156937"), new Make("foo"), new Model("baz"));
        assertThat(car.getCarID()).isEqualTo(new CarId(1));
        assertThat(car.getVin()).isEqualTo(new VIN("3FADP4EJ9BM156937"));
        assertThat(car.getMake()).isEqualTo(new Make("foo"));
        assertThat(car.getModel()).isEqualTo(new Model("baz"));
    }

    /**
     * Sample integration test
     */
    @Test
    public void should_RespondCarID_whenValidRequest() 
        throws Exception {
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"carID\":{\"value\":2}, \"vin\":{\"value\":\"3FADP4EJ9BM156937\"}, \"make\":{\"value\":\"foo\"}, \"model\":{\"value\":\"bar\"} }")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().string(is("Added car: " +
                    (new Car(new CarId(2), new VIN("3FADP4EJ9BM156937"), new Make("foo"), new Model("bar"))).toString())));
    }

    @Test
    public void should_Return_Error_whenInvalidRequest()
        throws Exception {
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"carID\":\"2\", \"vin\":\"wrong\", \"make\":\"foo\" ,\"model\":\"bar\" }")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void should_Reject_Null_CarID() {
        NullPointerException e = assertThrows(NullPointerException.class, () -> {
            new Car(null, new VIN("3FADP4EJ9BM156937"), new Make("Holden"), new Model("Commodore"));
        });
        assertEquals("CarId", e.getMessage());
    }

    @Test
    public void should_Reject_CarID_Out_Of_Range() {
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> {
            new Car(new CarId(100000001), new VIN("3FADP4EJ9BM156937"), new Make("Holden"), new Model("Commodore"));
        });
        assertEquals("CarId", e.getMessage());
    }

    @Test
    public void should_Reject_Null_VIN() {
        NullPointerException e = assertThrows(NullPointerException.class, () -> {
            new Car(new CarId(1), null, new Make("Holden"), new Model("Commodore"));
        });
        assertEquals("VIN", e.getMessage());
    }

    @Test
    public void should_Reject_Invalid_VIN() {
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> {
            new Car(new CarId(1), new VIN("Rubbish"), new Make("Holden"), new Model("Commodore"));
        });
        assertEquals("VIN", e.getMessage());
    }

    @Test
    public void should_Reject_Null_Make() {
        NullPointerException e = assertThrows(NullPointerException.class, () -> {
            new Car(new CarId(1), new VIN("3FADP4EJ9BM156937"), null, new Model("Commodore"));
        });
        assertEquals("Make", e.getMessage());
    }

    @Test
    public void should_Reject_Null_Model() {
        NullPointerException e = assertThrows(NullPointerException.class, () -> {
            new Car(new CarId(1), new VIN("3FADP4EJ9BM156937"), new Make("Holden"), null);
        });
        assertEquals("Model", e.getMessage());
    }

    @Test
    public void should_set_VRN() {
        Car car = new Car(new CarId(1), new VIN("3FADP4EJ9BM156937"), new Make("foo"), new Model("baz"));
        char[] vrnChars = new char[] { 'A', 'B', 'C', '1', '2', '3' };
        car.setVRN(vrnChars);
        for (char ch : vrnChars)
            assertEquals(ch, '\0');
        VRN vrn = car.getVRN();
        vrnChars = vrn.getVRN();
        assertEquals('A', vrnChars[0]);
        assertEquals('B', vrnChars[1]);
        assertEquals('C', vrnChars[2]);
        assertEquals('1', vrnChars[3]);
        assertEquals('2', vrnChars[4]);
        assertEquals('3', vrnChars[5]);
        assertThrows(IllegalStateException.class, vrn::getVRN);
    }

}
