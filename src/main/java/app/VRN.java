package app;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;

public final class VRN {

    // different JSON libraries have annotations for excluding members from serialization;
    // the actual one to be used will depend on the library
    @JsonIgnore
    private char[] vrn;

    public VRN(char[] vrn) {
        this.vrn = Arrays.copyOf(vrn, vrn.length);
    }

    public synchronized char[] getVRN() {
        if (vrn == null)
            throw new IllegalStateException("Repeat read of VRN");
        char[] result = Arrays.copyOf(vrn, vrn.length);
        Arrays.fill(vrn, '\0');
        vrn = null;
        return result;
    }

    @Override
    public String toString() {
        return "VRN";
    }

    private void readObject(ObjectInputStream inputStream) {
        throw new IllegalAccessError("Serialization not allowed");
    }

    private void writeObject(ObjectOutputStream outputStream) {
        throw new IllegalAccessError("Serialization not allowed");
    }

}

