package app;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Objects;

/**
 * Apache Commons Validator package has methods for 
 * various validations: https://commons.apache.org/proper/commons-validator/
 */

public class Car {

    /**
     * A local unique identifier
     */
    private CarId carID;

    /**
     * North America VIN standard 1981
     */
    private VIN vin;

    /**
     * Brand of the car (e.g. Audi)
     */
    private Make make;

    /**
     * A name used by manufacture to market a range
     * of similar cars (e.g. Q5)
     */
    private Model model;
    
    /**
     * Vehicle Registration Number Sensitive and unique.
     * private String vrn;
     */
    private VRN vrn;

    /**
     * Photo of the car.
     * private File photo;
     */

    public Car(CarId carID, VIN vin, Make make, Model model) {
        this.carID = Objects.requireNonNull(carID, "CarId");
        this.vin = Objects.requireNonNull(vin, "VIN");
        this.make = Objects.requireNonNull(make, "Make");
        this.model = Objects.requireNonNull(model, "Model");
    }

    public CarId getCarID() {
        return this.carID;
    }

    public VIN getVin() {
        return this.vin;
    }
    
    public Make getMake() {
        return this.make;
    }

    public Model getModel() {
        return this.model;
    }

    @Override
    public String toString() {
        return this.carID.toString() + " "  + this.vin + " " + this.make + " " + this.model;
    }

    public Car() {
        // no-arg constructor added because Jackson doesn't work without it
    }

    public final void setVRN(char[] vrn) {
        this.vrn = new VRN(vrn);
        Arrays.fill(vrn, '\0');
    }

    public final VRN getVRN() {
        return vrn;
    }

    private void readObject(ObjectInputStream inputStream) throws IOException {
        carID = new CarId(inputStream.readInt());
        vin = new VIN(inputStream.readUTF());
        make = new Make(inputStream.readUTF());
        model = new Model(inputStream.readUTF());
    }

    private void writeObject(ObjectOutputStream outputStream) throws IOException {
        outputStream.writeInt(carID.getValue());
        outputStream.writeUTF(vin.getValue());
        outputStream.writeUTF(make.getValue());
        outputStream.writeUTF(model.getValue());
    }

}
