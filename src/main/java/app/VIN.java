package app;

public class VIN {

    public static final int LENGTH = 17;
    public static final String VIN_PATTERN = "[0-9A-Z]*";

    private String value;

    public VIN(String value) {
        if (value == null)
            throw new NullPointerException("VIN");
        if (value.length() != LENGTH)
            throw new IllegalArgumentException("VIN");
        if (!value.matches(VIN_PATTERN))
            throw new IllegalArgumentException("VIN");
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof VIN))
            return false;
        return value.equals(((VIN)o).value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public String toString() {
        return value;
    }

    public VIN() {

    }
}
