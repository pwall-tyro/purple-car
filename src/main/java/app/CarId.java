package app;

import java.util.Objects;

public class CarId {

    public static final int MIN_VALUE = 1;
    public static final int MAX_VALUE = 99999999;

    private int value;

    public CarId(int value) {
        if (value < MIN_VALUE || value > MAX_VALUE)
            throw new IllegalArgumentException("CarId");
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof CarId))
            return false;
        return value == ((CarId)o).value;
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }

    public CarId() {

    }
}
