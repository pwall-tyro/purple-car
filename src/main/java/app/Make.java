package app;

public class Make {

    public static final int MIN_LENGTH = 1;
    public static final int MAX_LENGTH = 20;

    private String value;

    public Make(String value) {
        if (value == null)
            throw new NullPointerException("Make");
        if (value.length() < MIN_LENGTH || value.length() > MAX_LENGTH)
            throw new IllegalArgumentException("Make");
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Make))
            return false;
        return value.equals(((Make)o).value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public String toString() {
        return value;
    }

    public Make() {

    }
}
